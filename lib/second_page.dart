import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:camera_project/image_view.dart';
import 'package:get/get.dart';
import 'package:gallery_saver/gallery_saver.dart';

class SecondPage extends StatefulWidget {
  const SecondPage({super.key, required this.camera});
  final CameraDescription camera;

  @override
  State<SecondPage> createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  late CameraController cameraController;
  late Future<void> _initializeControllerFuture;

  void startCamera() async {
    cameraController = CameraController(
      widget.camera,
      ResolutionPreset.medium,
      enableAudio: false,
    );
    _initializeControllerFuture = cameraController.initialize();
  }

  @override
  void initState() {
    startCamera();
    super.initState();
  }

  @override
  void dispose() {
    cameraController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
          child: Scaffold(
        body: Stack(
          children: [
            FutureBuilder(
                future: _initializeControllerFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return CameraPreview(cameraController);
                  } else {
                    return const Center(child: CircularProgressIndicator());
                  }
                }),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.all(120),
                child: ElevatedButton(
                    onPressed: () async {
                      try {
                        await _initializeControllerFuture;
                        cameraController.setFlashMode(FlashMode.off);
                        final image = await cameraController.takePicture();
                        if (!mounted) return;
                        await GallerySaver.saveImage(image.path,
                            albumName: 'flutter');
                        Get.to(ImagePage(imagePath: image.path));
                      } catch (e) {
                        print(e);
                      }
                    },
                    child: const Text('Shot')),
              ),
            )
          ],
        ),
      )),
    );
  }
}
