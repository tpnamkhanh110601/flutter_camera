import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:camera/camera.dart';
import 'package:image_picker/image_picker.dart';

import 'package:camera_project/second_page.dart';
import 'package:camera_project/image_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  final firstCamera = cameras.first;
  runApp(MainApp(camera: firstCamera));
}

class MainApp extends StatefulWidget {
  MainApp({super.key, required this.camera});
  final CameraDescription camera;

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  Future<String> pickImage(ImageSource source) async {
    final image = await ImagePicker().pickImage(source: source);
    if (image == null) return '';
    return image.path;
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    onPressed: () {
                      Get.to(SecondPage(
                        camera: widget.camera,
                      ));
                    },
                    child: const Text('Camera')),
                ElevatedButton(
                    onPressed: () async {
                      String path = await pickImage(ImageSource.gallery);
                      if (path != '') {
                        Get.to(ImagePage(imagePath: path));
                      } else {
                        const Text('Failed to pick image');
                      }
                    },
                    child: const Text('Pick a picture'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
